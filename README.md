# systema [![pipeline status](https://gitlab.com/vise890/systema/badges/master/pipeline.svg)](https://gitlab.com/vise890/systema/commits/master)

Utilities for defining [Integrant](https://github.com/weavejester/integrant)
system configurations with [Aero](https://github.com/juxt/aero)-style EDN.

## [API Docs](https://vise890.gitlab.io/systema/)

## Usage

[![Clojars Project](https://img.shields.io/clojars/v/vise890/systema.svg)](https://clojars.org/vise890/systema)

```clojure
;;; project.clj
[vise890/systema "2018.06.06"]
```

Define a `resources/config_test.edn` like this:
```clojure
{;; Integrant `key`s are defined with `#ig/key`
 #ig/key :key1
 {;; `integrant.core.ref`s are defined with `#ig/ref`
  :key2 #ig/ref :key2
  ;; All Aero tag literals can be used
  :n    #ref [:n]}

 ;; keys not annotated with `#ig/key` won't be included in the read ig-config
 :n 42

 #ig/key :key2
 {:n #ref [:n]}}
```

Then:
```clojure
(ns systema.readme-test
  (:require [aero.core :as aero]
            [clojure.java.io :as io]
            [integrant.core :as ig]
            [systema.core :as sys]))

(defmethod ig/init-key :key1
  [_ {:keys [n key2]}]
  ,,,)

(defmethod ig/init-key :key2
  [_ {:keys [n]}]
  ,,,)

(def ig-config
  (-> "config_test.edn"
      io/resource
      (sys/read-ig-config {:profile :uat})))

ig-config
;; => {:key2 {:n 42}, :key1 {:key2 #integrant.core.Ref{:key :key2}, :n 42}}

(ig/init ig-config)



;; You can still use aero.core/read-config to get the whole thing:
(def config
  (-> "config_test.edn"
      io/resource
      (aero/read-config {:profile :uat})))

config
;; => {:key1 {:key2 #integrant.core.Ref{:key :key2}, :n 42}, :n 42, :key2 {:n 42}}
```

Have a look in [`config.sample.edn`](./config.sample.edn) for a more realistic
example.

## Motivation

More often than not, the structure of app configuration ends up mirroring the
structure of the Integrant system configuration.

This forces you to either:

- pass around the global config to every component and have each component
  extract the values it needs (this is error prone, and pretty much impossible
  to refactor); or
- putting component-specific values in their `opts` (which is what Systema
  facilitates).

## License

Copyright © 2018 Martino Visintin

Distributed under the Eclipse Public License either version 1.0 or (at your
option) any later version.
