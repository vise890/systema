(ns systema.core-test
  (:require [aero.core :as aero]
            [clojure.java.io :as io]
            [clojure.test :refer :all]
            [integrant.core :as ig]
            [systema.core :as sut]))

(deftest read-ig-config-test
  (testing "only keys defined with #ig/key are included in ig-config"
    (let [exp {:key1 {:n    42
                      :key2 (ig/ref :key2)
                      :keys (ig/refset :keys)}
               :key2 {:n 42}}

          got (-> "config_test.edn"
                  io/resource
                  sut/read-ig-config)]
      (is (= exp got)))))

(deftest aero-read-config-test
  (testing "all value are included in aero/read-config"
    (let [exp {:key1 {:n    42
                      :key2 (ig/ref :key2)
                      :keys (ig/refset :keys)}
               :n    42
               :key2 {:n 42}}
          got (-> "config_test.edn"
                  io/resource
                  aero/read-config)]
      (is (= exp got)))))

(deftest tagged-literals-test
  (let [c (-> "builtin_tag_literals.edn"
              io/resource
              aero/read-config)]
    (testing "re-pattern"
      (is (re-matches (:re-pattern c) "re-pattern"))
      (is (nil? (:re-pattern.nil c))))
    (testing "trim"
      (is (= "trim" (:trim c)))
      (is (nil? (:trim.nil c)))
      (is (= "trim" (:trim.+join c)))
      (testing "backwards-compatible trim on singleton vectors"
        (is (= "trim" (:trim.singleton c)))
        (is (= "trim" (:trim.singleton+join c)))))))
