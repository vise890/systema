(ns systema.readme-test
  (:require [aero.core :as aero]
            [clojure.java.io :as io]
            [integrant.core :as ig]
            [systema.core :as sys]))

(defmethod ig/init-key :key1
  [_ {:keys [n key2]}]
  ,,,)

(defmethod ig/init-key :key2
  [_ {:keys [n]}]
  ,,,)

(def ig-config
  (-> "config_test.edn"
      io/resource
      (sys/read-ig-config {:profile :uat})))

ig-config
;; => {:key2 {:n 42}, :key1 {:key2 #integrant.core.Ref{:key :key2}, :n 42}}

(ig/init ig-config)



;; You can still use aero.core/read-config to get the whole thing:
(def config
  (-> "config_test.edn"
      io/resource
      (aero/read-config {:profile :uat})))

config
;; => {:key1 {:key2 #integrant.core.Ref{:key :key2}, :n 42}, :n 42, :key2 {:n 42}}
