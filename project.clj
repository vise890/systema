(defproject vise890/systema "2019.03.08"

  :description "Utilities for defining Integrant system configurations with Aero-style EDN."

  :url "https://gitlab.com/vise890/systema"

  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[aero "1.1.3"]
                 [integrant "0.7.0"]
                 [org.clojure/clojure "1.10.0"]]

  :plugins [[lein-codox "0.10.6"]]

  :profiles {:dev {:resource-paths ["test/resources"]}
             :ci  {:deploy-repositories
                   [["clojars" {:url           "https://clojars.org/repo"
                                :username      :env
                                :password      :env
                                :sign-releases false}]]}})
