(ns systema.core
  (:require [aero.core :as aero]
            [clojure.string :as string]
            [integrant.core :as ig]))

(defmethod aero/reader 're-pattern [_ _ s]
  (some-> s re-pattern))

(defn- singleton? [xs] (and (coll? xs) (= 1 (count xs))))
(defn- singleton-get [x]
  (cond
    (singleton? x) (first x)
    :else x))
(defmethod aero/reader 'trim [_ _ s]
  (when-let [s* (singleton-get s)]
    (string/trim s*)))

(def ^:private ^:dynamic *ig-keys* nil)

(defmethod aero/reader 'ig/key [_ _ k]
  (when *ig-keys* (swap! *ig-keys* conj k))
  k)

(defmethod aero/reader 'ig/ref [_ _ value] (ig/ref value))
(defmethod aero/reader 'ig/refset [_ _ value] (ig/refset value))

(defn read-ig-config
  "Reads an Integrant config from `source`, returning only the keys marked with
  the tag `#ig/key`.

  Takes same arguments as `aero.core/read-config`."
  ([source]
   (read-ig-config source {}))
  ([source given-opts]
   (binding [*ig-keys* (atom #{})]
     (-> source
         (aero/read-config given-opts)
         (select-keys @*ig-keys*)))))
